<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/
Route::group(['middleware' => 'auth:sanctum'], function (){
    Route::resource('user', \App\Http\Controllers\Api\UserController::class)
        ->only([
            'show',
            'update',
        ]);
    Route::resource('post', \App\Http\Controllers\Api\PostController::class);
    Route::resource('comment', \App\Http\Controllers\Api\CommentController::class);
});
Route::post('user', [\App\Http\Controllers\Api\UserController::class, 'store']);
Route::post('auth', [\App\Http\Controllers\AuthController::class, 'auth']);
