<?php


namespace App\Services\Interfaces;


use App\Services\Interfaces\Actions\Create;
use App\Services\Interfaces\Actions\DeleteById;
use App\Services\Interfaces\Actions\GetAll;
use App\Services\Interfaces\Actions\GetOneById;
use App\Services\Interfaces\Actions\Update;
use Illuminate\Http\Request;

interface iPostService extends
    Create,
    Update,
    DeleteById,
    GetAll,
    GetOneById
{

}
