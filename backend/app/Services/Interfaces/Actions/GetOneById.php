<?php


namespace App\Services\Interfaces\Actions;


interface GetOneById
{
    public function getOneById(int $id);
}
