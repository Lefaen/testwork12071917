<?php


namespace App\Services\Interfaces\Actions;


use Illuminate\Http\Request;

interface Update
{
    public function update(Request $request, int $id);
}
