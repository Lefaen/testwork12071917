<?php


namespace App\Services\Interfaces\Actions;


use Illuminate\Http\Request;

interface Create
{
    public function create(Request $request);
}
