<?php


namespace App\Services\Interfaces\Actions;


interface DeleteById
{
    public function deleteById(int $id);
}
