<?php


namespace App\Services\Interfaces\Actions;



use Illuminate\Http\Request;

interface GetAllByPostId
{
    public function GetAllByPostId(int $id = null);
}
