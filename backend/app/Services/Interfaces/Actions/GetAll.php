<?php


namespace App\Services\Interfaces\Actions;


interface GetAll
{
    public function getAll();
}
