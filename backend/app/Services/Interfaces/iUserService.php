<?php


namespace App\Services\Interfaces;


use App\Services\Interfaces\Actions\Create;
use App\Services\Interfaces\Actions\GetOneById;
use App\Services\Interfaces\Actions\Update;

interface iUserService extends
    Create,
    Update,
    GetOneById
{

}
