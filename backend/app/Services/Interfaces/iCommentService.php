<?php


namespace App\Services\Interfaces;


use App\Services\Interfaces\Actions\Create;
use App\Services\Interfaces\Actions\DeleteById;
use App\Services\Interfaces\Actions\GetAllByPostId;

interface iCommentService extends
    Create,
    DeleteById,
    GetAllByPostId
{

}
