<?php


namespace App\Services;


use App\Models\User;
use App\Services\Interfaces\iUserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserService implements iUserService
{

    public function create(Request $request)
    {
        $data = $this->getDataFromRequest($request);
        User::create($data);
    }

    public function getOneById(int $id)
    {
        $user = User::find($id);
        return $user;
    }

    public function update(Request $request, int $id)
    {
        $data = $this->getDataFromRequest($request);
        $user = User::find($id);
        $user->update($data);
    }

    private function getDataFromRequest(Request $request): array
    {
        $request->except(['_token', '_method']);
        $data = $request->toArray();
        if($password = $request->get('password')){
            $data['password'] = Hash::make($password);
        }
        return $data;
    }
}
