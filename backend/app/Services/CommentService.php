<?php


namespace App\Services;


use App\Models\Comment;
use App\Services\Interfaces\iCommentService;
use Illuminate\Http\Request;

class CommentService implements iCommentService
{

    public function create(Request $request)
    {
        $data = $this->getDataFromRequest($request);
        Comment::create($data);
    }

    public function deleteById(int $id)
    {
        $comment = Comment::find($id);
        $comment->delete();
    }

    public function GetAllByPostId(int $id = null)
    {
        $comments = Comment::all();
        if ($id > 0) {
            $comments = $comments->where('post_id', $id);
        }
        return $comments;
    }

    private function getDataFromRequest(Request $request): array
    {
        $request->except(['_token', '_method']);
        $data = $request->toArray();
        return $data;
    }
}
