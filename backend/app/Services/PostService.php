<?php


namespace App\Services;


use App\Models\Post;
use App\Services\Interfaces\iPostService;
use Illuminate\Http\Request;

class PostService implements iPostService
{

    public function getAll()
    {
        $posts = Post::all();
        return $posts;
    }

    public function getOneById(int $id)
    {
        $post = Post::find($id);
        return $post;
    }

    public function create(Request $request)
    {
        $data = $this->getDataFromRequest($request);
        Post::create($data);
    }

    public function update(Request $request, int $id)
    {
        $data = $this->getDataFromRequest($request);
        $post = Post::find($id);
        $post->update($data);
    }

    public function deleteById(int $id)
    {
        $post = Post::find($id);
        $post->delete();
    }

    private function getDataFromRequest(Request $request): array
    {
        $request->except(['_token', '_method']);
        $data = $request->toArray();
        return $data;
    }
}
