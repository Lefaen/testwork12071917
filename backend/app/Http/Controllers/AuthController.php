<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function auth(Request $request)
    {
        $fields = $request->validate([
            'email' => 'required|string',
            'password' => 'required|string',
        ]);

        $user = User::where('email', $fields['email'])->first();
        if(!$user || !Hash::check($fields['password'], $user->password)) {
            return response([
                'message' => 'Bad credential',

            ], 401);
        }

        $user->tokens()->delete();
        $token = $user->createToken('login_token')->plainTextToken;

        $response = [
            'user_id' => $user->id,
            'token' => $token,
        ];

        return response($response, 201);
    }
}
