<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class AuthControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_auth()
    {
        $user = User::factory()->create();
        $password = 'password';
        $user->password = Hash::make($password);
        $user->save();

        $response = $this->json('POST', 'api/auth', [
            'email' => $user->email,
            'password' => $password,
        ]);

        $response->assertJsonStructure([
            'user_id',
            'token',
        ]);
    }
}
