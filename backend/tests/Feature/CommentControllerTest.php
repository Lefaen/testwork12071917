<?php

namespace Tests\Feature;

use App\Models\Comment;
use App\Models\Post;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CommentControllerTest extends TestCase
{
    use RefreshDatabase;

    private string $token;

    protected function setUp(): void
    {
        parent::setUp();
        $users = User::factory()->count(5)->create();
        Post::factory()->count(15)->create();
        $this->token = $users->random()->createToken('test')->plainTextToken;
    }

    public function test_index()
    {
        $post = Post::all()->random();
        $comments = Comment::factory()->count(30)->create();

        $response = $this->json('get', 'api/comment?post_id=' . $post->id, [], [
            'Authorization' => 'Bearer ' . $this->token,
        ]);
        $response->assertJsonStructure([
            '*' => [
                'id',
                'post_id',
                'text',
            ]
        ]);
        $response->assertStatus(200);
    }

    public function test_store()
    {
        $comment = Comment::factory()->make();

        $response = $this->json('post', 'api/comment', $comment->toArray(), [
            'Authorization' => 'Bearer ' . $this->token,
        ]);

        $this->assertDatabaseHas('comments', $comment->toArray());
        $response->assertStatus(201);
    }

    public function test_destroy()
    {
        $comment = Comment::factory()->create();

        $response = $this->json('delete', 'api/comment/' . $comment->id, [], [
            'Authorization' => 'Bearer ' . $this->token,
        ]);

        $this->assertDeleted('comments', $comment->toArray());
        $response->assertStatus(204);
    }
}
