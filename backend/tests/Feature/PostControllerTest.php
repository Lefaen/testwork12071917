<?php

namespace Tests\Feature;

use App\Models\Post;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PostControllerTest extends TestCase
{
    use RefreshDatabase;

    private string $token;

    protected function setUp(): void
    {
        parent::setUp();
        $users = User::factory()->count(10)->create();
        $this->token = $users->random()->createToken('test')->plainTextToken;
    }

    public function test_index()
    {
        $posts = Post::factory()->count(30)->create();

        $response = $this->json('get','api/post', [], [
            'Authorization' => 'Bearer ' . $this->token,
        ]);

        $response->assertJsonStructure([
            '*' => [
                'id',
                'user_id',
                'text',
            ]
        ]);
        $response->assertStatus(200);
    }

    public function test_store()
    {
        $post = Post::factory()->make();

        $response = $this->json('post', 'api/post', $post->toArray(), [
            'Authorization' => 'Bearer ' . $this->token,
        ]);

        $this->assertDatabaseHas('posts', $post->toArray());
        $response->assertStatus(201);
    }

    public function test_show()
    {
        $post = Post::factory()->create();

        $response = $this->json('get', 'api/post/' . $post->id, [], [
            'Authorization' => 'Bearer ' . $this->token,
        ]);

        $response->assertJsonStructure([
            'id',
            'user_id',
            'text',
        ]);
        $response->assertStatus(200);
    }

    public function test_update()
    {
        $post = Post::factory()->create();
        $newPost = Post::factory()->make();

        $response = $this->json('put', 'api/post/' . $post->id, $newPost->toArray(), [
            'Authorization' => 'Bearer ' . $this->token,
        ]);

        $post->refresh();

        foreach ($newPost->toArray() as $key => $value) {
            $this->assertEquals($value, $post->$key);
        }
        $response->assertStatus(204);
    }

    public function test_destroy()
    {
        $post = Post::factory()->create();

        $response = $this->json('delete', 'api/post/' . $post->id, [], [
            'Authorization' => 'Bearer ' . $this->token,
        ]);

        $this->assertDeleted('posts', $post->toArray());
        $response->assertStatus(204);
    }
}
