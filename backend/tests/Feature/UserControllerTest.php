<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_store()
    {
        $user = User::factory()->make();
        $user->password = 'password';

        $response = $this->json('post', 'api/user',
            $user->only([
                'name',
                'email',
                'password'
            ])
        );

        $this->assertDatabaseHas('users', $user->only([
            'name',
            'email',
        ]));
        $response->assertStatus(201);
    }

    public function test_update()
    {
        $user = User::factory()->create();
        $token = $user->createToken('test')->plainTextToken;
        $newUser = User::factory()->make();
        $newUser->password = 'password';

        $response = $this->json('put', 'api/user/' . $user->id, $newUser->only([
            'name',
            'email',
            'password'
        ]), [
            'Authorization' => 'Bearer ' . $token,
        ]);

        $user->refresh();

        $response->assertStatus(204);
        $this->assertEquals($newUser->only('name', 'email'), $user->only(['name', 'email']));
    }

    public function test_show()
    {
        $user = User::factory()->create();
        $token = $user->createToken('test')->plainTextToken;

        $response = $this->json('get', 'api/user/' . $user->id, [], [
            'Authorization' => 'Bearer ' . $token,
        ]);


        $response->assertJsonStructure([
            'id',
            'name',
            'email',
        ]);
        $response->assertStatus(200);
    }
}
