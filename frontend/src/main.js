// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import axios from 'axios'
import VueAxios from 'vue-axios'

Vue.use(VueAxios, axios)
Vue.config.productionTip = false

const apiHost = 'http://crud.homestead/'
if (typeof apiHost !== 'undefined') {
  Vue.axios.defaults.baseURL = apiHost
  Vue.axios.defaults.headers = {
    Authorization: 'Bearer ' + localStorage.token
  }
}
router.beforeEach((to, from, next) => {
  if (to.name !== 'Login' && to.name !== 'Register' && !localStorage.token) next({ name: 'Login' })
  else next()
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  data () {
    return {
      auth: false
    }
  },
  mounted () {
    this.isAuth()
  },
  methods: {
    isAuth: function () {
      this.auth = Boolean(localStorage.token)
      return this.auth
    }
  },
  router,
  template: '<App/>',
  components: { App }
})
