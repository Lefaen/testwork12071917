import Vue from 'vue'
import Router from 'vue-router'
import PostList from '../components/PostList'
import PostOne from '../components/PostOne'
import Login from '../components/Login'
import Register from '../components/Register'
import Profile from '../components/Profile'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
    {
      path: '/post',
      name: 'Post',
      component: PostList
    },
    {
      path: '/post/:id',
      name: 'PostOne',
      component: PostOne
    },
    {
      path: '/profile',
      name: 'Profile',
      component: Profile
    }
  ]
})
